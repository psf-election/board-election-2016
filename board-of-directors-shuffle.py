import collections
import random

Candidate = collections.namedtuple('Candidate', 'name statement_url')

CANDIDATES = [
    Candidate(
        'Lorena Mesa',
        'https://wiki.python.org/moin/PythonSoftwareFoundation/BoardCandidates2016#lorena-mesa'),
    Candidate(
        'Trey Hunner',
        'https://wiki.python.org/moin/PythonSoftwareFoundation/BoardCandidates2016#trey-hunner'),
    Candidate(
        'Jackie Kazil',
        'https://wiki.python.org/moin/PythonSoftwareFoundation/BoardCandidates2016#jackie-kazil'),
    Candidate(
        'Naomi Ceder',
        'https://wiki.python.org/moin/PythonSoftwareFoundation/BoardCandidates2016#naomi-ceder'),
    Candidate(
        'Carol Willing',
        'https://wiki.python.org/moin/PythonSoftwareFoundation/BoardCandidates2016#carol-willing'),
    Candidate(
        'Philip James',
        'https://wiki.python.org/moin/PythonSoftwareFoundation/BoardCandidates2016#philip-james'),
    Candidate(
        'Carrie Anne Philbin',
        'https://wiki.python.org/moin/PythonSoftwareFoundation/BoardCandidates2016#carrie-anne-philbin'),
    Candidate(
        'Younggun Kim',
        'https://wiki.python.org/moin/PythonSoftwareFoundation/BoardCandidates2016#younggun-kim'),
    Candidate(
        'Diana Clarke',
        'https://wiki.python.org/moin/PythonSoftwareFoundation/BoardCandidates2016#diana-clarke'),
    Candidate(
        'Van Lindberg',
        'https://wiki.python.org/moin/PythonSoftwareFoundation/BoardCandidates2016#van-lindberg'),
    Candidate(
        'Chris Clifton',
        'https://wiki.python.org/moin/PythonSoftwareFoundation/BoardCandidates2016#chris-clifton'),
    Candidate(
        'Annapoornima Koppad',
        'https://wiki.python.org/moin/PythonSoftwareFoundation/BoardCandidates2016#annapoornima-koppad'),
    Candidate(
        'Monty Taylor',
        'https://wiki.python.org/moin/PythonSoftwareFoundation/BoardCandidates2016#monty-taylor'),
    Candidate(
        'Raphael Pierzina',
        'https://wiki.python.org/moin/PythonSoftwareFoundation/BoardCandidates2016#raphael-pierzina'),
    Candidate(
        'Kushal Das',
        'https://wiki.python.org/moin/PythonSoftwareFoundation/BoardCandidates2016#kushal-das'),
    Candidate(
        'Raghav Hanumantharau',
        'https://wiki.python.org/moin/PythonSoftwareFoundation/BoardCandidates2016#kushal-das'),
    Candidate(
        'Ed Leafe',
        'https://wiki.python.org/moin/PythonSoftwareFoundation/BoardCandidates2016#ed-leafe'),
    Candidate(
        'Simon Cross',
        'https://wiki.python.org/moin/PythonSoftwareFoundation/BoardCandidates2016#simon-cross'),
    Candidate(
        'Massimo Di Pierro',
        'https://wiki.python.org/moin/PythonSoftwareFoundation/BoardCandidates2016#massimo-di-pierro'),
    Candidate(
        'James Powell',
        'https://wiki.python.org/moin/PythonSoftwareFoundation/BoardCandidates2016#james-powell'),
    Candidate(
        'Don Sheu',
        'https://wiki.python.org/moin/PythonSoftwareFoundation/BoardCandidates2016#don-sheu'),
]

ROW_TEMPLATE = (
    '<tr><td><a href="%(url)s">%(name)s</a></td><td>{{%(opt_name)s}} yes</td>'
    '<td>{{%(opt_name)s}} no</td><td>{{%(opt_name)s!}} abstain</td></tr>'
)


def candidate_table_row(candidate):
    option_name = candidate.name.replace(' ', '_')
    return ROW_TEMPLATE % {
        'name': candidate.name,
        'url': candidate.statement_url,
        'opt_name': option_name,
    }


random.shuffle(CANDIDATES)
for candidate in CANDIDATES:
    print(candidate_table_row(candidate))
